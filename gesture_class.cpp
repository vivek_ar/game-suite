#include "gesture_class.hpp"
using namespace std;
using namespace cv;
void gestureControl::handRegions(Mat img)
{

// leftrect.x = faceCenter.x - 2.5 * faceWidth;
// leftrect.y = faceCenter.y - 2.5 * faceHeight;
// leftrect.width = 2.5 * faceWidth
// leftrect = 2.5 * faceHeight;

leftRect.x = (faceRect.x - (3 * faceRect.width));
if(leftRect.x < 0)
leftRect.x = 0;
leftRect.y = faceRect.y;
leftRect.width = min((3 * faceRect.width),  faceRect.x);
leftRect.height = 2 * faceRect.height;
if((leftRect.y + leftRect.height) > (img.rows-1))
leftRect.height = img.rows - 1 - leftRect.y; //leftRect.height - (leftRect.y + leftRect.height - img.rows - 1);

 //- faceRect.height/2;// + faceRect.height/2 - (2.5 * faceRect.height));


rightRect.x = (faceRect.x + faceRect.width);
rightRect.y = faceRect.y; // - faceRect.height/2;

rightRect.width = 3 * faceRect.width;
if((rightRect.x + rightRect.width) > img.cols)
rightRect.width = img.cols -1 - rightRect.x; //rightRect.width - (rightRect.x + rightRect.width - img.cols - 1);

rightRect.height = 2 * faceRect.height;
if((rightRect.y + rightRect.height) > img.rows)
rightRect.height =  img.rows - 1 - rightRect.y;//rightRect.height - (rightRect.y + rightRect.height - img.rows - 1);

circle(img,Point(leftRect.x, leftRect.y),5,Scalar(150,150,150),5);
circle(img,Point(rightRect.x, rightRect.y),5,Scalar(150,150,150),5);
rectangle(img, leftRect, cv::Scalar(0,0,0), 2);
rectangle(img, rightRect, cv::Scalar(255,255,0), 2);
rectangle(img, faceRect, cv::Scalar(255,255,255), 2);

circle(img,Point(faceRect.x+faceRect.width,faceRect.y+faceRect.height),5,Scalar(0,250,0),5);

leftIm = img(Rect(leftRect));
rightIm = img(Rect(rightRect));
cout<< "here" << endl;
imshow("leftIm", leftIm);
imshow("rightIm", rightIm);
imshow("img", img);
waitKey(1);

}

void gestureControl::handGesture(Mat img)
{
    if( !hand_cascade.load( hand_cascade_name ))
    {
      cout<<"Failed to open cascade file\n";
      exit(0);
    }
    foundHand[0] = 0;
    foundHand[1] = 0;   
    Mat imgGray;
    std::vector<Rect> handObjects;
    cvtColor( img, imgGray, COLOR_BGR2GRAY );
    equalizeHist( imgGray, imgGray);

    handRegions(imgGray);
    for(int p = 1; p < 3; p++)
      if(p % 2 == 0)
          {
          flip(rightIm, rightIm, 1);
          hand_cascade.detectMultiScale( rightIm, handObjects, 1.1, 10, 0, Size(40, 40));
          if(handObjects.size()>0)
          foundHand[1]= 1;
          }
    else
    {
          hand_cascade.detectMultiScale( leftIm, handObjects, 1.1, 10, 0, Size(40, 40));
          if(handObjects.size()>0)
          foundHand[0] = 1;
    }

    cout << "right   " << foundHand[0] << "    left   " << foundHand[1] << endl;
    //return foundHand;
}

//-------------------------------------------------------------------------------------------//
//TAKE FilterData

void gestureControl::filterData()
{

  if(upXY.size()>0)
  {
    int tempX = 10000,tempY = 10000;
    for(int i = 0;i<upXY.size();i++)
    {
      if(tempY>upXY[i].y)
      {
        tempY = upXY[i].y;
        tempX = upXY[i].x;
      }
    }
    upMost = Point(int(tempX),int(tempY));
  }

  if(leftXY.size()>0)
  {
    int tempX = 0,tempY = 0;
    for(int i = 0;i<leftXY.size();i++)
    {
      tempX += leftXY[i].x;
      tempY += leftXY[i].y;
    }
    leftMost = Point(int(tempX/leftXY.size()),int(tempY/leftXY.size()));
  }

  if(rightXY.size()>0)
  {
    int tempX = 0,tempY = 0;
    for(int i = 0;i<rightXY.size();i++)
    {
      tempX += rightXY[i].x;
      tempY += rightXY[i].y;
    }
    rightMost = Point(int(tempX/rightXY.size()),int(tempY/rightXY.size()));
  }

  if(middleXY.size()>0)
  {
    int tempX = 0,tempY = 0;
    for(int i = 0;i<middleXY.size();i++)
    {
      tempX += middleXY[i].x;
      tempY += middleXY[i].y;
    }
    middleMost = Point(int(tempX/middleXY.size()),int(tempY/middleXY.size()));
  }
}



//-------------------------------------------------------------------------------------------//
//TAKE UP POSITION

void gestureControl::upPos()
{

  std::system("clear");

  cout<<"\n Stand still here for ... \n";
  double startTime = (double)getTickCount();
  double endTime =(double)getTickCount();
  double ms = 0;


  Mat tempFrame,tempGray;
  objects.clear();

  while(ms<CALIBTIME)
  {

    //--------------------------------------------------//
    //CAMERA OPEN - CLOSE

    cap>>tempFrame;
    resize(tempFrame,tempFrame,Size(tempFrame.cols/SCALEFACTOR,tempFrame.rows/SCALEFACTOR));
    cvtColor( tempFrame, tempGray, COLOR_BGR2GRAY );
    equalizeHist( tempGray, tempGray);

    face_cascade.detectMultiScale( tempGray, objects, 1.1, 8, 0, Size(20, 20));

    for( size_t i = 0; i < objects.size(); i++ )
    {
      if(norm(tempFrame(objects[i]),background(objects[i]))>DISTHRESH)
      {
        upXY.push_back(Point(objects[i].x,objects[i].y));
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);

        circle(tempFrame,Point(tempFrame.cols/2,tempFrame.rows/2),5,Scalar(250,0,250),5);

        cv::rectangle(tempFrame, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(0,0,255), 2);
      }
      else
      {
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
        circle(tempFrame,Point(tempFrame.cols/2,tempFrame.rows/2),5,Scalar(250,0,0),5);

        cv::rectangle(tempFrame, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(255,0,55), 2);
      }

    }

    imshow("frame",tempFrame);
    //imshow("background",background);
    if(waitKey(1)>0)
      exit(0);



      endTime =(double)getTickCount();
      ms = (endTime-startTime)/getTickFrequency();

      if(int(ms)%1==0)
      {
        std::system("clear");
        cout<<"\n \t\t\t\tTaking UP position \n";
        cout<<"\n \tKeep jumping here for : "<<int((CALIBTIME-ms))<<" ms \n";
      }


    }
    destroyAllWindows();

}




//-------------------------------------------------------------------------------------------//
//TAKE MIDDLE POSITION

void gestureControl::rightPos()
{

  std::system("clear");

  cout<<"\n Stand still here for ... \n";
  double startTime = (double)getTickCount();
  double endTime =(double)getTickCount();
  double ms = 0;


  Mat tempFrame,tempGray;
  objects.clear();

  while(ms<CALIBTIME)
  {

    //--------------------------------------------------//
    //CAMERA OPEN - CLOSE

    cap>>tempFrame;
    resize(tempFrame,tempFrame,Size(tempFrame.cols/SCALEFACTOR,tempFrame.rows/SCALEFACTOR));
    cvtColor( tempFrame, tempGray, COLOR_BGR2GRAY );
    equalizeHist( tempGray, tempGray);

    face_cascade.detectMultiScale( tempGray, objects, 1.1, 8, 0, Size(20, 20));

    for( size_t i = 0; i < objects.size(); i++ )
    {
      if(norm(tempFrame(objects[i]),background(objects[i]))>DISTHRESH)
      {
        rightXY.push_back(Point(objects[i].x,objects[i].y));
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
        circle(tempFrame,Point(tempFrame.cols/2,tempFrame.rows/2),5,Scalar(250,0,250),5);

        cv::rectangle(tempFrame, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(0,0,255), 2);
      }
      else
      {
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
        circle(tempFrame,Point(tempFrame.cols/2,tempFrame.rows/2),5,Scalar(250,0,0),5);

        cv::rectangle(tempFrame, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(255,0,55), 2);
      }

    }

    imshow("frame",tempFrame);
    //imshow("background",background);
    if(waitKey(1)>0)
      exit(0);



      endTime =(double)getTickCount();
      ms = (endTime-startTime)/getTickFrequency();

      if(int(ms)%1==0)
      {
        std::system("clear");
        cout<<"\n \t\t\t\t\t\tTaking RIGHT position \n";
        cout<<"\n \tStand still here for : "<<int((CALIBTIME-ms))<<" ms \n";
      }


    }
    destroyAllWindows();

}


  //-------------------------------------------------------------------------------------------//
  //TAKE LEFT POSITION

  void gestureControl::leftPos()
{

  std::system("clear");

  cout<<"\n Stand still here for ... \n";
  double startTime = (double)getTickCount();
  double endTime =(double)getTickCount();
  double ms = 0;


  Mat tempFrame,tempGray;
  objects.clear();

  while(ms<CALIBTIME)
  {

    //--------------------------------------------------//
    //CAMERA OPEN - CLOSE

    cap>>tempFrame;
    resize(tempFrame,tempFrame,Size(tempFrame.cols/SCALEFACTOR,tempFrame.rows/SCALEFACTOR));
    cvtColor( tempFrame, tempGray, COLOR_BGR2GRAY );
    equalizeHist( tempGray, tempGray);

    face_cascade.detectMultiScale( tempGray, objects, 1.1, 8, 0, Size(20, 20));

    for( size_t i = 0; i < objects.size(); i++ )
    {
      if(norm(tempFrame(objects[i]),background(objects[i]))>DISTHRESH)
      {
        leftXY.push_back(Point(objects[i].x+objects[i].width,objects[i].y+objects[i].height));
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);

        circle(tempFrame,Point(tempFrame.cols/2,tempFrame.rows/2),5,Scalar(250,0,250),5);
        cv::rectangle(tempFrame, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(0,0,255), 2);
      }
      else
      {
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
        circle(tempFrame,Point(tempFrame.cols/2,tempFrame.rows/2),5,Scalar(250,0,0),5);

        cv::rectangle(tempFrame, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(255,0,55), 2);
      }
    }

    imshow("frame",tempFrame);
    //imshow("background",background);

    if(waitKey(1)>0)
      exit(0);



      endTime =(double)getTickCount();
      ms = (endTime-startTime)/getTickFrequency();

      if(int(ms)%1==0)
      {
        std::system("clear");
        cout<<"\n \t\tTaking LEFT position \n";
        cout<<"\n \tStand still here for : "<<int((CALIBTIME-ms))<<" ms \n";
      }


    }
    destroyAllWindows();

}


  //-------------------------------------------------------------------------------------------//
  //TAKE MIDDLE POSITION

void gestureControl::middlePos()
{

  std::system("clear");

  cout<<"\n Stand still here for ... \n";
  double startTime = (double)getTickCount();
  double endTime =(double)getTickCount();
  double ms = 0;


  Mat tempFrame,tempGray;
  objects.clear();

  while(ms<CALIBTIME)
  {

    //--------------------------------------------------//
    //CAMERA OPEN - CLOSE

    cap>>tempFrame;
    resize(tempFrame,tempFrame,Size(tempFrame.cols/SCALEFACTOR,tempFrame.rows/SCALEFACTOR));
    cvtColor( tempFrame, tempGray, COLOR_BGR2GRAY );
    equalizeHist( tempGray, tempGray);

    face_cascade.detectMultiScale( tempGray, objects, 1.1, 8, 0, Size(20, 20));

    for( size_t i = 0; i < objects.size(); i++ )
    {

      if(norm(tempFrame(objects[i]),background(objects[i]))>DISTHRESH)
      {
        middleXY.push_back(Point(objects[i].x+objects[i].width/2,objects[i].y+objects[i].height/2));
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
        circle(tempFrame,Point(tempFrame.cols/2,tempFrame.rows/2),5,Scalar(250,0,250),5);

        cv::rectangle(tempFrame, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(0,0,255), 2);
      }
      else
      {
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
        circle(tempFrame,Point(tempFrame.cols/2,tempFrame.rows/2),5,Scalar(250,0,0),5);

        cv::rectangle(tempFrame, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(255,0,55), 2);
      }
    }

    imshow("frame",tempFrame);
    //imshow("background",background);

    if(waitKey(1)>0)
      exit(0);



      endTime =(double)getTickCount();
      ms = (endTime-startTime)/getTickFrequency();

      if(int(ms)%1==0)
      {
        std::system("clear");
        cout<<"\n \t\t\t\tTaking MIDDLE position \n";
        cout<<"\n \tStand still here for : "<<int((CALIBTIME-ms))<<" ms \n";
      }


    }
    destroyAllWindows();

}

void gestureControl::singlePlayerMode()
{

  cout << " \n One player come to the front \n";
  /* CODE */
  cout<< "\n Come to the middle position and stand still \n";

  cout<< "\n Initializing... \n";

  double startTime = (double)getTickCount();
  double endTime = (double)getTickCount();
  double ms = 0;

  while(ms<STARTTIME)
  {


    endTime =(double)getTickCount();
    ms = (endTime-startTime)/getTickFrequency();

    if(abs(ms-int(ms))<0.1)
    {
      std::system("clear");

      cout << " One player come to the front \n";
      /* CODE */
      cout<< "\n Come to the middle position and stand still \n";

      cout<< "\n Initializing... \n";

      cout<<"\n Starting in : "<<int((STARTTIME-ms))<<" ms \n";
    }
  }

  middlePos();

  //---------------------------------------------------------------------------//
  startTime = (double)getTickCount();
  endTime = (double)getTickCount();
  ms = 0;

  while(ms<STARTTIME/2.0)
  {


    endTime =(double)getTickCount();
    ms = (endTime-startTime)/getTickFrequency();

    if(abs(ms-int(ms))<0.1)
    {
      std::system("clear");

      cout<< "\n Be ready to jump on the middle position \n";

      cout<< "\n Initializing... \n";

      cout<<"\n Starting in : "<<int((STARTTIME/2.0-ms))<<" ms \n";
    }
  }


  upPos();

  //---------------------------------------------------------------------------//
  startTime = (double)getTickCount();
  endTime = (double)getTickCount();
  ms = 0;

  while(ms<STARTTIME/2.0)
  {


    endTime =(double)getTickCount();
    ms = (endTime-startTime)/getTickFrequency();

    if(abs(ms-int(ms))<0.1)
    {
      std::system("clear");

      cout<< "\n Move to the rightmost position and stand still \n";

      cout<< "\n Initializing... \n";

      cout<<"\n Starting in : "<<int((STARTTIME/2.0-ms))<<" ms \n";
    }
  }


  rightPos();
  //---------------------------------------------------------------------------//
  startTime = (double)getTickCount();
  endTime = (double)getTickCount();
  ms = 0;

  while(ms<STARTTIME/2.0)
  {


    endTime =(double)getTickCount();
    ms = (endTime-startTime)/getTickFrequency();

    if(abs(ms-int(ms))<0.1)
    {
      std::system("clear");

      cout<< "\n Move to the leftmost position and stand still \n";

      cout<< "\n Initializing... \n";

      cout<<"\n Starting in : "<<int((STARTTIME/2.0-ms))<<" ms \n";
    }
  }

  leftPos();

  //cout<<"\n Elapsed Time : "<<ms/1000<<" ms \n";



  filterData();

}

void gestureControl::doublePlayerMode()
{
  
  cout << " First player come to the front on the left side \n";
  /* CODE */
	//singlePlayerMode();
  cout << " Second player come to the front on the right side \n";
  /* CODE */
    //singlePlayerMode();
}


int gestureControl::calibration()
{

  std::system("clear");
  switch(numPlayers)
{
  case 1: {
    cout<< " Entering Single Player Mode \n";
    singlePlayerMode();
    break;
  }
  case 2: {
    cout<< " Entering Double Player Mode \n";
    //doublePlayerMode();
    break;
  }

  default : cout<< " Not a supported mode \n"; return 0;
}
}


int gestureControl::process()
{
  std::system("clear");

  cout<< "LEFT  : "<< leftMost<<endl<<endl;
  cout<< "RIGHT  : "<< rightMost<<endl;

  Mat cropGray, frameHand;
  objects.clear();
  while(cap.isOpened())
  {

    cap>>frame;
    frameHand = frame.clone();
    resize(frame,frame,Size(frame.cols/SCALEFACTOR,frame.rows/SCALEFACTOR));

    //circle(frame,middleMost,2,Scalar(0,250,0),2);

    //cout<< "Frame  : "<< frame.cols<<" x "<<frame.rows<<endl;
    Rect ROI(rightMost.x,upMost.y,leftMost.x-rightMost.x,frame.rows-upMost.y);
    cropped = frame(ROI);
    backCropped = background(ROI);



    cvtColor( cropped, cropGray, COLOR_BGR2GRAY );
    equalizeHist( cropGray, cropGray);

    face_cascade.detectMultiScale( cropGray, objects, 1.1, 4, 0, Size(20, 20));

    for( size_t i = 0; i < objects.size(); i++ )
    {

      if(norm(cropped(objects[i]),backCropped(objects[i]))>DISTHRESH)// FOUND NEW RECTANGLES
      {
        if(objects.size()==1)
        {
          //circle(frame,Point((2.0*middleMost.x+1.0*leftMost.x)/3.0,middleMost.y),2,Scalar(250,0,0),2);
          //circle(frame,Point((2.0*middleMost.x+1.0*rightMost.x)/3.0,middleMost.y),2,Scalar(0,0,250),2);
          //circle(frame,Point((objects[i].x+rightMost.x+objects[i].width/2),middleMost.y),5,Scalar(0,0,0),5);

          circle(frame,Point((objects[i].x + rightMost.x + objects[i].width/2), upMost.y + objects[i].y + objects[i].height/2), 5, Scalar(250,0,250), 5);
          //circle(frame, faceCenter, 2, Scalar(150,0,150), 2);

          if((rightMost.x+objects[i].x+objects[i].width/2)>(2.0*middleMost.x+1.0*leftMost.x)/3.0)
          {
            DIRX = "LEFT";
            DIRSCRIPT[0] = -1;

            Rect indicator(frame.cols-frame.cols/10,0,frame.cols/10,frame.rows);
            cv::rectangle(frame, indicator, cv::Scalar(0,255,0), -1);

          }
          else if((rightMost.x+objects[i].x+objects[i].width/2)<(2.0*middleMost.x+1.0*rightMost.x)/3.0)
          {
            DIRX = "RIGHT";
            DIRSCRIPT[0] = 1;

            Rect indicator(0,0,frame.cols/10,frame.rows);
            cv::rectangle(frame, indicator, cv::Scalar(0,255,0), -1);
          }
          else
          {
            DIRX = "NULL";
            DIRSCRIPT[0] = 0;

          }

          if((upMost.y+objects[i].y+objects[i].height/2)>(2.0*middleMost.y+1.0*(2*middleMost.y-upMost.y))/3.0)
          {
            DIRY = "DOWN";
            DIRSCRIPT[1] = 1;

            Rect indicator(0,frame.rows-frame.rows/10,frame.cols,frame.rows/10);
            cv::rectangle(frame, indicator, cv::Scalar(0,5,255), -1);
          }
          else if((upMost.y+objects[i].y+objects[i].height/2)<(2.0*middleMost.y+1.0*upMost.y)/3.0)
          {
            DIRY = "UP";
            DIRSCRIPT[1] = -1;

            Rect indicator(0,0,frame.cols,frame.rows/10);
            cv::rectangle(frame, indicator, cv::Scalar(0,5,255), -1);
          }
          else
          {
            DIRY = "NULL";
            DIRSCRIPT[1] = 0;

          }

        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
        circle(frame,Point(frame.cols/2,frame.rows/2),5,Scalar(0,250,0),5);
        cv::rectangle(cropped, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(0,255,0), 2);
        
          //faceRect.tl() = Point(center.x-objects[i].width/2,center.y-objects[i].height/2);
          //faceRect.br() = Point(center.x+objects[i].width/2,center.y+objects[i].height/2);
        faceRect.x = (objects[i].x + rightMost.x ) * SCALEFACTOR;
        faceRect.y = ( upMost.y + objects[i].y ) * SCALEFACTOR;
        faceRect.width = objects[i].width * SCALEFACTOR;
        faceRect.height = objects[i].height * SCALEFACTOR;
        //rectangle(frame, faceRect, cv::Scalar(0,255,255), -1);

        //cout << "*****here*****" << endl;
        //foundHand = handGesture(frameHand);
        handGesture(frameHand);
        string text;
        int fontFace = CV_FONT_HERSHEY_SIMPLEX;
        double fontScale = 1;
        int thickness = 1;  
        cv::Point textOrg(250, 250);

        if (foundHand[0]==0 && foundHand[1] == 0)
        
          text = "Nothing Detected";

        else if (foundHand[0]==1 && foundHand[1] == 0)
        
          text = "Right Hand Detected";
        

        else if (foundHand[0]==0 && foundHand[1] == 1)
          
          text = "Left Detected";

        else

          text = "Both Detected";

        putText(frame, text, textOrg, fontFace, fontScale, Scalar::all(255), thickness,8);
        //putText(frame, const string& text, Point org, int fontFace, double fontScale, Scalar color, int thickness=1, int lineType=8, bool bottomLeftOrigin=false )
        

      }
      else // FOUND MULTIPLE RECTANGLES
      {
        Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
        cv::rectangle(cropped, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(0,0,255), 2);
        circle(frame,Point(frame.cols/2,frame.rows/2),5,Scalar(0,0,250),5);
      }
    }
    else //FOUND BACKGROUND MATCHING RECTANGLES
    {
      Point center( objects[i].x + objects[i].width/2, objects[i].y + objects[i].height/2);
      circle(frame,Point(frame.cols/2,frame.rows/2),5,Scalar(250,0,0),5);
      cv::rectangle(cropped, Point(center.x-objects[i].width/2,center.y-objects[i].height/2),Point(center.x+objects[i].width/2,center.y+objects[i].height/2), cv::Scalar(255,0,55), 2);
    }

  }

  std::system("clear");
  cout<<"DIRECTION - X : " <<DIRX<<"  |  Y : "<<DIRY<<endl;


  resize(frame,frame,Size(890,480));
  flip(frame,frame,1);
  imshow("frame",frame);
  //imshow("frame",cropped);

  //ostringstream command;
  //command<<"sh game.sh "<<DIRSCRIPT[0]<<" "<<DIRSCRIPT[1];

  //string comm = command.str();

  //system(comm.c_str());
  if(waitKey(1)>0)
    exit(0);
  }

  //system("sh game.sh 10 10");
  return 0;
}
