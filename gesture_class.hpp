#include <opencv2/opencv.hpp>
#include <thread>
#include <mutex>

#include <iostream>

using namespace std;
using namespace cv;

#define STARTTIME 3.0
#define CALIBTIME 2.0
#define SCALEFACTOR 1.5
#define DISTHRESH 3000

class gestureControl{

public:

  String face_cascade_name = "/home/cube26/opencv/smartTV/data/lbpcascade_frontalface.xml";
  String hand_cascade_name = "/home/cube26/Desktop/cascade_new.xml"; //vivek's variable
  CascadeClassifier face_cascade;
  CascadeClassifier hand_cascade; //vivek's variable
  mutex m;

  Point ROI[2];

  Mat frame;
  Mat cropped;
  Mat backCropped;
  Mat imgGrayRight, imgGrayLeft; //vivek's variable

  Mat frame_gray; /* grayscale image */
  std::vector<Rect> objects;
  std::vector<Point> path;

  std::vector<Point> upXY;
  std::vector<Point> rightXY;
  std::vector<Point> middleXY;
  std::vector<Point> leftXY;

  Point upMost;
  Point leftMost;
  Point rightMost;
  Point middleMost;
  Point faceCenter; //vivek's variable
  Point leftHand;   //vivek's variable
  Point rightHand;  //vivek's variable

  Rect leftRect, rightRect, faceRect;
//int faceHeight, faceWidth; //vivek's variable

  Mat background;
  Mat leftIm = Mat::zeros(Size(640,480), CV_32FC3);
  Mat rightIm = Mat::zeros(Size(640,480), CV_32FC3);

  float dirCountX = 0,dirCountY = 0;
  int foundHand[2] = {0, 0};
  String DIRX = "NULL";
  String DIRY = "NULL";
  int DIRSCRIPT[2] = {0,0};
  int numPlayers = 0;
  int camIndex = 0;
  VideoCapture cap;

public:
  gestureControl(){
    if( !face_cascade.load( face_cascade_name ))
    {
      cout<<"Failed to open cascade file\n";
      exit(0);
    }

    cap.open(camIndex);
    if(cap.isOpened())
    {
      for(int i=0;i<5;i++)
      {
        cap>>background;
        resize(background,background,Size(background.cols/SCALEFACTOR,background.rows/SCALEFACTOR));

      }
    }

    leftMost = Point(0,0);
    rightMost = Point(background.cols,background.rows);
    middleMost = Point(background.cols/2,background.rows/2);

  };
  ~gestureControl(){};
  void filterData();
  void upPos();
  void leftPos();
  void rightPos();
  void middlePos();
  void singlePlayerMode();
  void doublePlayerMode();
  int calibration();
  int process();
  void handGesture(Mat);
  void handRegions(Mat);
};