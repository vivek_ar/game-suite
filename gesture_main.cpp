
#include "gesture_class.hpp"

using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
  int a = 0;
  gestureControl obj;
  obj.numPlayers = atoi(argv[2]);
  obj.camIndex = atoi(argv[1]);
  obj.calibration();
  obj.process();
  return 0;
}